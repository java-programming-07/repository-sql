package com.jevillac07.repository.sql.util;

import com.jevillac07.repository.sql.model.Sale;
import lombok.Data;

import java.util.List;

/**
 * Sale list utility.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Data
public class SaleList {
  private List<Sale> saleList;
}
