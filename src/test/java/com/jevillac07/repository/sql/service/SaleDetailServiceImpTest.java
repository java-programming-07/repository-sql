package com.jevillac07.repository.sql.service;

import com.jevillac07.repository.sql.model.SaleDetail;
import com.jevillac07.repository.sql.util.ReadFile;
import com.jevillac07.repository.sql.util.SaleList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SaleDetailServiceImpTest {
  @Autowired
  protected SaleDetailService saleDetailService;

  @Autowired
  protected SaleService saleService;

  @BeforeAll
  void savingSales() {
    SaleList saleList = ReadFile.readDataFromFile("mock/sale-list-insert.json", SaleList.class);
    saleList.getSaleList().forEach(
      sale -> sale.getSaleDetailList().forEach(
        saleDetail -> saleDetail.setSale(sale)
      )
    );

    saleList.getSaleList().forEach(sale -> saleService.saveCrud(sale));
  }

  @AfterAll
  void deletingSales() {
    saleService.deleteAllRecordsCrud();
  }

  @Test
  @Order(1)
  @DisplayName("Find sale detail rows by sale id and pagination")
  void findBySaleIdWithPaging() {
    Integer page = 0;
    Integer size = 2;
    String sort = "id";
    // Get the first position in the list
    Long saleId = saleService.findAllCrud().get(0).getId();
    Integer expectedSaleDetail = 2;

    List<SaleDetail> saleDetailList = saleDetailService.findBySaleIdWithPaging(saleId, page, size, sort);

    assertEquals(expectedSaleDetail, saleDetailList.size());
  }

  @Test
  @Order(2)
  @DisplayName("Find a sale detail with a description")
  void findByDescription() {
    String description = "Table";
    Integer expectedRows = 6;

    List<SaleDetail> saleDetailList = saleDetailService.findByDescription(description);

    assertEquals(expectedRows, saleDetailList.size());
  }

  @Test
  @Order(3)
  @DisplayName("Find a sale detail with sale id and description")
  void findBySaleIdAndDescription() {
    // Get the first position in the list
    Long saleId = saleService.findAllCrud().get(0).getId();
    String description = "Mouse";
    Integer expectedRows = 1;

    List<SaleDetail> saleDetailList = saleDetailService.findBySaleIdAndDescription(saleId, description);

    assertEquals(expectedRows, saleDetailList.size());
  }
}
