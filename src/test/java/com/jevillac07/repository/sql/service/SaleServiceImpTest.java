package com.jevillac07.repository.sql.service;

import com.jevillac07.repository.sql.model.Sale;
import com.jevillac07.repository.sql.util.ReadFile;
import com.jevillac07.repository.sql.util.SaleList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SaleServiceImpTest {
  @Autowired
  protected SaleService saleService;

  @BeforeAll
  void savingSales() {
    SaleList saleList = ReadFile.readDataFromFile("mock/sale-list-insert.json", SaleList.class);
    saleList.getSaleList().forEach(
      sale -> sale.getSaleDetailList().forEach(
        saleDetail -> saleDetail.setSale(sale)
      )
    );

    saleList.getSaleList().forEach(sale -> saleService.saveCrud(sale));
  }

  @AfterAll
  void deletingSales() {
    saleService.deleteAllRecordsCrud();
  }

  @Test
  @Order(1)
  @DisplayName("Save a sale with crud repository")
  void saveSaleWithCrud() {
    Sale saleRequest = ReadFile.readDataFromFile("mock/save-sale-crud-request.json", Sale.class);
    saleRequest.getSaleDetailList().get(0).setSale(saleRequest);
    Integer sizeSaleDetail = saleRequest.getSaleDetailList().size();

    Sale sale = saleService.saveCrud(saleRequest);

    assertEquals(sizeSaleDetail, sale.getSaleDetailList().size());
  }

  @Test
  @Order(2)
  @DisplayName("Get a sale with repository paging")
  void getSaleWithPaging() {
    Integer page = 1;
    Integer size = 5;
    String sort = "id";
    Integer expectedRows = 1;

    List<Sale> sale = saleService.getSaleWithPaging(page, size, sort);

    assertEquals(expectedRows, sale.size());
  }
}
