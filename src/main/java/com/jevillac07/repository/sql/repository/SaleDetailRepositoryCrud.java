package com.jevillac07.repository.sql.repository;

import com.jevillac07.repository.sql.model.SaleDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Sale detail using crud repository.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Repository
public interface SaleDetailRepositoryCrud
  extends CrudRepository<SaleDetail, Long> {
  /**
   * findBySaleId: find a sale detail slice by sale id and pageable.
   * @param saleId sale id
   * @param pageable pageable object
   * @return a sale detail slice
   * */
  Slice<SaleDetail> findBySaleId(Long saleId, Pageable pageable);

  /**
   * findByDescription: find a sale detail list by a description.
   * @param description description
   * @return a sale detail list
   * */
  List<SaleDetail> findByDescription(String description);

  /**
   * findBySaleIdAndDescription: find a sale detail list by sale id
   * and description.
   * @param saleId sale id
   * @param description description
   * @return a sale detail list
   * */
  List<SaleDetail> findBySaleIdAndDescription(Long saleId, String description);
}
