package com.jevillac07.repository.sql.repository;

import com.jevillac07.repository.sql.model.Sale;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Sale detail using crud repository.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Repository
public interface SaleRepositoryCrud extends CrudRepository<Sale, Long> {
}
