package com.jevillac07.repository.sql.repository;

import com.jevillac07.repository.sql.model.Sale;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Sale using paging repository.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Repository
public interface SaleRepositoryPaging
  extends PagingAndSortingRepository<Sale, Long> {
}
