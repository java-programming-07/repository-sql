package com.jevillac07.repository.sql.service;

import com.jevillac07.repository.sql.model.SaleDetail;

import java.util.List;

/**
 * Sale detail interface.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
public interface SaleDetailService {
  /**
   * findBySaleIdWithPaging: find a sale detail by sale id in the database.
   * @param saleId id of the sale
   * @param page pagination position
   * @param size number of rows displayed
   * @param sort sort list by parameter
   * @return a sale details list
   */
  List<SaleDetail> findBySaleIdWithPaging(
    Long saleId, Integer page, Integer size, String sort);

  /**
   * findByDescription: fin by description.
   * @param description detail description
   * @return a sale details list
   */
  List<SaleDetail> findByDescription(String description);

  /**
   * findBySaleIdAndDescription: find by sales id and description.
   * @param saleId id of the sale
   * @param description detail description
   * @return a sale details list
   */
  List<SaleDetail> findBySaleIdAndDescription(Long saleId, String description);
}
