package com.jevillac07.repository.sql.service;

import com.jevillac07.repository.sql.model.Sale;

import java.util.List;

/**
 * Sale interface.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
public interface SaleService {
  /**
   * saveCrud: save a sale in the database.
   * @param sale a sale object
   * @return a sale
   */
  Sale saveCrud(Sale sale);

  /**
   * deleteAllRecordsCrud: delete all records.
   */
  void deleteAllRecordsCrud();

  /**
   * findAllCrud: find all records.
   * @return a list of sale
   */
  List<Sale> findAllCrud();

  /**
   * getSaleWithPaging: Get a sale with pagination.
   * @param page pagination position
   * @param size number of rows displayed
   * @param sort sort list by parameter
   * @return a list of sale
   */
  List<Sale> getSaleWithPaging(Integer page, Integer size, String sort);
}
