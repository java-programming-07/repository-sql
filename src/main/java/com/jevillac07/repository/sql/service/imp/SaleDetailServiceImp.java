package com.jevillac07.repository.sql.service.imp;

import com.jevillac07.repository.sql.model.SaleDetail;
import com.jevillac07.repository.sql.repository.SaleDetailRepositoryCrud;
import com.jevillac07.repository.sql.service.SaleDetailService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Sale detail service implement.
 * @author Juan Villaorduna
 * @version 2022/09/02
 */
@Service
@AllArgsConstructor
public final class SaleDetailServiceImp implements SaleDetailService {
  /** Sales detail repository with Crud. */
  private final SaleDetailRepositoryCrud saleDetailRepositoryCrud;

  @Override
  public List<SaleDetail> findBySaleIdWithPaging(
    final Long saleId, final Integer page,
    final Integer size, final String sort) {
    Pageable paging = PageRequest.of(page, size, Sort.by(sort).ascending());

    Slice<SaleDetail> saleDetailSlice = saleDetailRepositoryCrud.findBySaleId(
      saleId, paging);

    return saleDetailSlice.getContent();
  }

  @Override
  public List<SaleDetail> findByDescription(final String description) {
    return saleDetailRepositoryCrud.findByDescription(description);
  }

  @Override
  public List<SaleDetail> findBySaleIdAndDescription(
    final Long saleId, final String description) {
    return saleDetailRepositoryCrud.findBySaleIdAndDescription(
      saleId, description);
  }
}
