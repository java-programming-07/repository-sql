package com.jevillac07.repository.sql.service.imp;

import com.jevillac07.repository.sql.model.Sale;
import com.jevillac07.repository.sql.repository.SaleRepositoryCrud;
import com.jevillac07.repository.sql.repository.SaleRepositoryPaging;
import com.jevillac07.repository.sql.service.SaleService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Sale service implementation.
 * @author Juan Villaorduna
 * @version 2022/09/02
 */
@Service
@AllArgsConstructor
public final class SaleServiceImp implements SaleService {
  /** Sales repository with Crud. */
  private final SaleRepositoryCrud saleRepositoryCrud;

  /** Sales repository with Paging. */
  private final SaleRepositoryPaging saleRepositoryPaging;

  @Override
  public Sale saveCrud(final Sale sale) {
    return saleRepositoryCrud.save(sale);
  }

  @Override
  public void deleteAllRecordsCrud() {
    saleRepositoryCrud.deleteAll();
  }

  @Override
  public List<Sale> findAllCrud() {
    return StreamSupport.stream(
      saleRepositoryCrud.findAll().spliterator(), false)
      .collect(Collectors.toList());
  }

  @Override
  public List<Sale> getSaleWithPaging(final Integer page, final Integer size,
                                      final String sort) {
    Pageable paging = PageRequest.of(page, size, Sort.by(sort).ascending());

    return saleRepositoryPaging.findAll(paging).getContent();
  }
}
