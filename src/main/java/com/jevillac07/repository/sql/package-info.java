/**
 * This project is about the different ways to connect a repository.
 * <p>
 * Crud and Paging interfaces to connect the repository.
 * We'll use database like h2, mysql, postgres, sql, oracle and mongodb.
 * </p>
 *
 * @author Juan Villaorduna
 * @version 1.0.1
 * @since 1.0.0
 */
package com.jevillac07.repository.sql;
