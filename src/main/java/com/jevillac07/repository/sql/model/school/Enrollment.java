package com.jevillac07.repository.sql.model.school;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

/**
 * Enrollment model.
 * @author Juan Villaorduna
 * @version 2022/09/25
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Enrollment {
  /** Enrollment id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Student id. */
  @ManyToOne
  @MapsId("studentId")
  private Student student;

  /** Course id. */
  @ManyToOne
  @MapsId("courseId")
  private Course course;

  /** enrollment creation date. */
  private String createAt;
}
