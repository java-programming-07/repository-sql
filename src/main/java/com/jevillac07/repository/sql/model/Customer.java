package com.jevillac07.repository.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * Customer model.
 * @author Juan Villaorduna
 * @version 2022/09/25
 */
@Entity
@IdClass(CustomerId.class)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
  /** Document type. */
  @Id
  private String documentType;

  /** Document number. */
  @Id
  private String documentNumber;

  /** Customer name. */
  private String name;

  /** Promotion list. */
  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @ToString.Exclude
  private Set<Promotion> promotions;
}
