package com.jevillac07.repository.sql.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * CustomerId model.
 * @author Juan Villaorduna
 * @version 2022/09/25
 */
@Getter
@Setter
@ToString
public class CustomerId implements Serializable {
  /** Document type. */
  private String documentType;

  /** Document number. */
  private String documentNumber;
}
