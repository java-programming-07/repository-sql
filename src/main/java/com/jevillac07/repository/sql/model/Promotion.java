package com.jevillac07.repository.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * Promotion model.
 * @author Juan Villaorduna
 * @version 2022/09/25
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Promotion {
  /** Promotion id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Promotion description. */
  private String description;

  /** Start date. */
  private String startDate;

  /** End date. */
  private String endDate;

  /** Customer list. */
  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,
    mappedBy = "promotions")
  @ToString.Exclude
  private Set<Customer> customers;
}
