/**
 * School model.
 *
 * @author Juan Villaorduna
 * @version 1.0.1
 * @since 1.0.0
 */
package com.jevillac07.repository.sql.model.school;
