package com.jevillac07.repository.sql.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Invoice model.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Entity
@Getter
@Setter
@ToString
public class Invoice {
  /** Invoice id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Document type. */
  private String documentType;

  /** Document serial. */
  private String documentSerial;

  /** Document number. */
  private String documentNumber;
}
