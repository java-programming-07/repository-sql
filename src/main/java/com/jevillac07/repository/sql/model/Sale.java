package com.jevillac07.repository.sql.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * Sale model.
 * @author Juan Villaorduna
 * @version 2022/09/02
 */
@Entity
@Getter
@Setter
@ToString
public class Sale {
  /** Sale id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Payment type. */
  private String paymentType;

  /** Paid date (dd/mm/yyyy). */
  private String paidDate;

  /** total sale amount. */
  private Double totalAmount;

  /** Invoice for sale. */
  @OneToOne(cascade = CascadeType.ALL)
  private Invoice invoice;

  /** Sale detail list. */
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,
    mappedBy = "sale")
  @ToString.Exclude
  private List<SaleDetail> saleDetailList;
}
