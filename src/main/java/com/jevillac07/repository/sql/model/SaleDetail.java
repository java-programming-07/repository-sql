package com.jevillac07.repository.sql.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

/**
 * Sale detail model.
 * @author Juan Villaorduna
 * @version 2017/5/24
 */
@Entity
@Getter
@Setter
@ToString
public class SaleDetail {
  /** Sale detail id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Description. */
  private String description;

  /** Quantity. */
  private Integer quantity;

  /** Price. */
  private Double price;

  /** Sale object. */
  @ManyToOne
  private Sale sale;
}
